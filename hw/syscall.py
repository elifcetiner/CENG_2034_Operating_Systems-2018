import os, time, signal

def uFunction():
    print("uFunction is completed , task successfully with the help of the child process!! ")

print("Parent pid: ", os.getpid() , "\n")

for i in range(1, 10):
    cpid = os.fork()
    if cpid == 0: 
        uselessFunction() 
        print("Child",i ,"pid: ", os.getpid())
        os.kill(os.getpid(), signal.SIGKILL)
        
    else:
        time.sleep(1)

os.kill(os.getpid(), signal.SIGKILL) 
